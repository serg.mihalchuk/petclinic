package com.example.petclinic.services;

import com.example.petclinic.entity.Vet;

import java.util.Collection;

public interface VetService extends CrudService<Vet, Long> {

    Collection<Vet> findBySpec(String spec);

}

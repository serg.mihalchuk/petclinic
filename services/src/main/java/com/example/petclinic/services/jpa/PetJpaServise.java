package com.example.petclinic.services.jpa;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.services.PetService;
import com.example.petclinic.services.config.JpaImlementation;
import org.springframework.data.jpa.repository.JpaRepository;

@JpaImlementation
public class PetJpaServise extends AbstractJpaService<Pet, Long>
        implements PetService {

    @Override
    public JpaRepository<Pet, Long> getRepository() {

        throw new UnsupportedOperationException();
    }

    @Override
    public Pet findByOwner(Owner owner) {

        throw new UnsupportedOperationException();
    }
}

package com.example.petclinic.services.map;

import com.example.petclinic.entity.BaseEntity;
import com.example.petclinic.services.BaseMapServise;
import com.example.petclinic.services.CrudService;

import java.util.Collection;
import java.util.Map;

public abstract class AbstractMapService<T extends BaseEntity<ID>, ID> implements CrudService<T, ID>, BaseMapServise<T, ID> {
    @Override
    public T findById(ID id) {
        return (T) getResource().get(id);
    }

    @Override
    public void save(T entity) {
//        getResource().put(entity, entity.getId());

    }

    @Override
    public Collection<T> findAll() {
        return (Collection<T>) getResource().values();
    }

    @Override
    public void delete(ID id) {
        getResource().remove(id);

    }

}

package com.example.petclinic.services;

import java.util.Map;

public interface BaseMapServise<T, ID> {

    Map<ID, T> getResource();

}

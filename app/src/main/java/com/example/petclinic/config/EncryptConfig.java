package com.example.petclinic.config;

import com.ulisesbocchio.jasyptspringboot.encryptor.SimplePBEByteEncryptor;
import com.ulisesbocchio.jasyptspringboot.encryptor.SimplePBEStringEncryptor;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PBEByteEncryptor;
import org.jasypt.salt.SaltGenerator;
import org.jasypt.salt.StringFixedSaltGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EncryptConfig {

    @Bean("customEncryptor")
    public StringEncryptor stringEncryptor(PBEByteEncryptor delegate){
        return new SimplePBEStringEncryptor(delegate);
    }

    @Bean
    public PBEByteEncryptor pbeByteEncryptor(SaltGenerator saltGenerator){
        SimplePBEByteEncryptor encryptor = new SimplePBEByteEncryptor();
        encryptor.setPassword("jasypt_pwd");
        encryptor.setAlgorithm("PBEWithMD5AndDES");
        encryptor.setIterations(2);
        encryptor.setSaltGenerator(saltGenerator);


        return encryptor;
    }

    @Bean
    public SaltGenerator saltGenerator (){
        return new StringFixedSaltGenerator("jasypt_salt");
    }


}

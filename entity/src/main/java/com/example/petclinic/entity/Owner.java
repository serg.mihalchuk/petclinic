package com.example.petclinic.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Owner implements BaseEntity<Long>{

    private Long id;
    private String firstName;
    private String lastName;
    private String phone;

}

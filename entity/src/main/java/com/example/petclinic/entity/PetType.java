package com.example.petclinic.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PetType {

    CAT("Cat"),
    DOG("Dog");

    private final String value;

}
